<%@include file="/apps/bootcamp-assignments/components/global.jspx" %><%
%><cq:defineObjects/><%@page session="false" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"  import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.foundation.ELEvaluator,
                    com.day.cq.commons.Externalizer" %><%
    // read the redirect target from the 'page properties' and perform the
    // redirect if WCM is disabled.
    String location = properties.get("redirectTarget", "");
    // resolve variables in path
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
    boolean wcmModeIsDisabled = WCMMode.fromRequest(request) == WCMMode.DISABLED;
    boolean wcmModeIsPreview = WCMMode.fromRequest(request) == WCMMode.PREVIEW;
    if ( (location.length() > 0) && ((wcmModeIsDisabled) || (wcmModeIsPreview)) ) {
        // check for recursion
        if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
            
            if(!slingRequest.getHeader("User-Agent").contains("OmtrBot")) {

                if(location != null && location.startsWith("/content/")) {
                    location = location.split("/content")[1];
                }

                response.sendRedirect(location + ".html");
            }
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return; 
    }

%><cq:include script="base.sightly.html"/>
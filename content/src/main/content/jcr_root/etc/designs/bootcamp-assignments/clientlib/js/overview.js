// var searchHasRun = false;
//
// app.controller('overviewController', function($scope, $http) {
//
//     var searchPath 	 		= '';
//     var locale       		= '';
//     var type     	 		= '';
//     var limit        		= 0;     // result items per page
//     var offset       		= 0;     // pointer on resultset
//     var navPageMax          = 5;     // maximum number of page nr to display on pager
//     var pageCount           = 0;
//
//     $scope.total     		= 0;
//     $scope.results   		= 0;
//     $scope.currentPageNr    = 1;
//     $scope.navPageNumbers   = new Array();
//     $scope.resultItems 		= new Array();
//     $scope.countries        = new Array();
//     $scope.selectedCountry  = '';
//     $scope.showCookieLink   = false;
//     $scope.cookieCountry  	= '';
//     $scope.cookieDisplayCountry	= '';
//
//     function getResults() {
//
//         searchUrl = "/bin/corporate/overview?" +
//                     "locale=" + locale +
//                     "&type=" + type +
//                     "&offset=" + offset +
//                     "&limit=" + limit +
//                     "&path=" + searchPath;
//
//     	// filter for country
//         if ($scope.selectedCountry && $scope.selectedCountry!='international') {
//         	searchUrl = searchUrl + "&ct=" + $scope.selectedCountry;
//         }
//
//         // alert(searchUrl);
//
//         $http.get(searchUrl).success(function(response) {
//             $scope.total = response.total;
//             $scope.results = response.results;
//            	$scope.resultItems = response.hits;
//             $scope.navPageNumbers = getNavPageNumbers();
//
// 			if(response.hits.length > 0) {
// 				$('#noSearchResults').hide();
// 			} else {
// 				$('#noSearchResults').show();
// 			}
//
// 			if(searchHasRun) {
// 				var offset = $(".overview").offset().top;
//
// 				if ($(window).width() >= 767) {
// 					offset = offset - 100;
// 				}
//
// 				$('html, body').animate({
// 					scrollTop: offset
// 				}, 1000);
// 			} else {
// 				searchHasRun = true;
// 			}
//         });
//     }
//
//     function getNavPageNumbers() {
//     	var pageNumbers = new Array();
//     	if ($scope.total>0) {
//         	pageCount = Math.ceil($scope.total / limit);
//         	for (var i = 1; i <= pageCount && i<=navPageMax; i++) {
//         		pageNumbers.push(i);
//         	}
//     	}
//     	return pageNumbers;
//     }
//
//     function getRequestParam(paramName) {
//         var paramValue = '';
//         var requestParamStr = location.search;
//
//         // get search param from request params
//         if (requestParamStr.indexOf("?")==0) {
//             requestParamStr = requestParamStr.slice(1);
//         }
//         var params = requestParamStr.split("&");
//         for (var i = 0; i < params.length; i++) {
//             var pair = params[i].split('=');
//             if (pair[0]==paramName) {
//                paramValue = pair[1];
//                break;
//             }
//         }
//         return paramValue;
//     }
//
//     /**  replaced angular/chosen select by sightly dropdown, see below
//     function initCountryFields() {
//
//       	// get country tags list for country dropdown
//         searchUrl = "/bin/corporate/tagvalues.country." +
//         			locale + ".pathlabel.1.text.json";
//
// 		$http.get(searchUrl).success(function(response) {
//
// 			$scope.countries = response;
//
// 			// add international option at first position
// 			$scope.countries.unshift({text:"International", value:"international"});
//
// 			$scope.selectedCountry = 'international';  // set default for country dropdown
//
// 			// set country link field from cookie
// 	    	var cookieCountry = Cookies.get('amplexor-overview-country');
//
// 			if (typeof(cookieCountry) !== "undefined") {
// 			    $scope.showCookieLink = true;
// 			    $scope.cookieCountry  = cookieCountry;
// 			    $scope.cookieDisplayCountry = getCountryText(cookieCountry);
// 			}
//
// 		});
//     }
//
//     function getCountryText(countryValue) {
//     	for (var i=0; i < $scope.countries.length; i++) {
//     		if (countryValue==$scope.countries[i].value) {
//     			return $scope.countries[i].text;
//     		}
//     	}
//     }
//
//     $scope.setCountryFilter = function() {
//
//     	// store new country selected to cookie
//     	if ($scope.selectedCountry!='international') {
//     		Cookies.set('amplexor-overview-country', $scope.selectedCountry, { expires: 365 });
//     	} else {
//     		Cookies.remove('amplexor-overview-country');
//     	}
//
//     	// reset search result
//         offset = 0;
//         $scope.resultItems = new Array();
//
//         // new search
//     	getResults();
//     }
//     **/
//
//     $scope.setCountryFilter = function(country) {
//
//         if (country) {
//         	$scope.selectedCountry = country;
//
//         	// store new country selected to cookie
//         	if ($scope.selectedCountry!='international') {
//         		Cookies.set('amplexor-overview-country', $scope.selectedCountry, { expires: 365 });
//         	} else {
//         		Cookies.remove('amplexor-overview-country');
//         	}
//
//         	// reset search result
//             offset = 0;
//             $scope.currentPageNr = 1;
//             $scope.resultItems = new Array();
//
//             // new search
//             getResults();
//         }
//     };
//
//     /*
//     $scope.$apply($scope.cookieLinkClicked = function() {
//     	// update country dropdown
//     	$scope.selectedCountry = $scope.cookieCountry;
//     	// filter by country
//     	$scope.setCountryFilter();
//     });
//
//     $scope.cookieLinkClicked = function() {
//         // update country dropdown
//     	$scope.selectedCountry = $scope.cookieCountry;
//     	//$scope.$apply();
//     	//$scope.$digest();
//
//     	// filter by country
//     	$scope.setCountryFilter();
//     }
//     */
//
//     $scope.navPageLinkClicked = function(pageNr) {
//         $scope.currentPageNr = pageNr;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.navPrevClicked = function() {
//     	$scope.currentPageNr = $scope.currentPageNr - 1;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.navNextClicked = function() {
//     	$scope.currentPageNr = $scope.currentPageNr + 1;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.hasPrevPage = function() {
//     	return ($scope.currentPageNr > 1) ? true : false;
//     }
//
//     $scope.hasNextPage = function() {
//     	return ($scope.currentPageNr < pageCount) ? true : false;
//     }
//
//     $scope.showPager = function() {
//     	return (pageCount > 1) ? true : false;
//     }
//
//     $scope.init = function(pLimit, pType, pLocale, pSearchPath) {
//     	limit  		 = parseInt(pLimit);
//     	type   		 = pType;
//     	locale 		 = pLocale;
//     	searchPath 	 = pSearchPath;
//     	//initCountryFields();  // init country dropdown and set country link from cookie
//     	getResults();
//     }
//
//     angular.element(document).ready(function () {
//     	$('#country-select').val('international');
//     });
//
// });
 
package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.amplexor.bootcamp.assignments.services.util.Constants;
import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.util.*;

/*
 * Servlet to get all tags as json (correct json to use in a select box)
 * based on a namespace and with a possible depth.
 * As selector, first give the namespace of the tags you want,
 * if needed you can add a depth. Then all children are also listed to the given depth
 */
@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.paths", value = {"/bin/corporate/tagvalues"})
})
public class TagValuesServlet extends AbstractValuesServlet {

    private static final long serialVersionUID = -8801798524447341021L;

    private final static Logger logger = LoggerFactory.getLogger(TagValuesServlet.class);

    public static final String TAG_FILTER_TYPE = "tag";

    @Reference(bind = "bindTagFilterFactory", unbind = "unbindTagFilterFactory", referenceInterface = TagFilterFactory.class, policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE)
    private Set<TagFilterFactory> tagFilterFactories = new HashSet<TagFilterFactory>();

    protected void bindTagFilterFactory(TagFilterFactory tagFilter) {
        synchronized (tagFilterFactories) {
            if (tagFilter != null) {
                tagFilterFactories.add(tagFilter);
            }
        }
    }

    protected void unbindTagFilterFactory(TagFilterFactory tagFilter) {
        synchronized (tagFilterFactories) {
            if (tagFilter != null) {
                tagFilterFactories.remove(tagFilter);
            }
        }
    }

    @Override
    protected ValuesRequest createValuesRequest(SlingHttpServletRequest request) {
        return TagValuesRequest.create(request);
    }

    private TagManager getTagManager(SlingHttpServletRequest request) {
        if (request != null) {
            ResourceResolver rr = request.getResourceResolver();
            if (rr != null) {
                return rr.adaptTo(TagManager.class);
            }
        }
        return null;
    }


    private Filter<Tag> getTagFilter(TagValuesRequest valuesRequest) {
        String suffix = valuesRequest.getSuffix();
        ResourceResolver rr = valuesRequest.getRequest().getResourceResolver();
        Set<Filter<Tag>> tagFilters = new HashSet<Filter<Tag>>();
        for (TagFilterFactory tagFilterFactory : tagFilterFactories) {
            Filter<Tag> tagFilter = tagFilterFactory.create(suffix, rr);
            if (tagFilter != null) {
                tagFilters.add(tagFilter);
            }
        }
        return new CompositeTagFilter(tagFilters);
    }

    protected Tag getRootTag(TagValuesRequest request, TagManager tagManager) {
        // Note that namespace id's end with a ':' character.
        if (tagManager == null) {
            return null;
        }
        Tag rootTag = tagManager.resolve(request.getRootTag());
        if (rootTag != null) {
            return rootTag;
        }
        return null;
    }

    private List<Tag> getTagValues(Tag startTag, int depth, int start, Filter<Tag> tagFilter) {
        List<Tag> tags = new ArrayList<Tag>();
        if (startTag == null || tagFilter == null) {
            return tags;
        }
        if (start <= depth) {
            for (Iterator<Tag> it = startTag.listChildren(tagFilter); it.hasNext(); ) {
                Tag childTag = it.next();
                tags.add(childTag);
                tags.addAll(getTagValues(childTag, depth, start + 1, tagFilter));
            }
        }
        return tags;
    }

    private Locale getLocale(TagValuesRequest request) {
        if (request != null && !StringUtils.isBlank(request.getLanguage())) {
            try {
                return new Locale(request.getLanguage());
            } catch (NullPointerException exc) {
                logger.warn("Could not create Locale from the specified language, defaulting to English.");
                return new Locale(Constants.EN_LANGUAGE);
            }
        }
        return new Locale(Constants.EN_LANGUAGE);
    }

    protected List<Map<String, String>> getValues(ValuesRequest vr) {
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        TagValuesRequest valuesRequest = (TagValuesRequest) vr;

        TagManager tagManager = getTagManager(valuesRequest.getRequest());

        Tag rootTag = getRootTag(valuesRequest, tagManager);
        if (rootTag == null)
            return results;
        Filter<Tag> tagFilter = getTagFilter(valuesRequest);
        List<Tag> tags = getTagValues(rootTag, valuesRequest.getDepth(), 1, tagFilter);
        Locale locale = getLocale(valuesRequest);
        for (Tag tag : tags) {
            results.add(getResultMap(valuesRequest, rootTag, locale, tag));
        }
        return results;
    }

    private Map<String, String> getResultMap(TagValuesRequest valuesRequest, Tag rootTag, Locale locale, Tag tag) {
        Map<String, String> result = new HashMap<String, String>();
        if (rootTag == null || locale == null || valuesRequest == null) {
            return result;
        }
        String namespaceLabel = findNamespaceLabel(rootTag, locale);
        if (namespaceLabel == null) {
            return result;
        }
        String labelText = createFormattedLabelText(valuesRequest, locale, tag, namespaceLabel);
        String labelId = createLabelId(valuesRequest,tag);
        result.put(valuesRequest.getLabelId(), labelId);
        result.put(valuesRequest.getLabelText(), labelText);
        return result;
    }

    private String createLabelId(TagValuesRequest valuesRequest, Tag tag) {
        if(Constants.NAMESPACE.equals(valuesRequest.getValueFormaType())) {
            return tag.getNamespace().getName() + ":" + tag.getLocalTagID();
        }
        if(Constants.TITLE.equals(valuesRequest.getValueFormaType())) {
            return tag.getTitle();
        }
            return tag.getLocalTagID();
    }

    private String findNamespaceLabel(Tag rootTag, Locale locale) {
        return rootTag.getNamespace().getTitle(locale);
    }

    private String createFormattedLabelText(TagValuesRequest valuesRequest, Locale locale, Tag tag, String namespaceLabel) {
        String labelText = createLabelText(valuesRequest, locale, tag, namespaceLabel);

        labelText = formatLabelText(valuesRequest, tag, labelText);
        return labelText;
    }

    private String formatLabelText(TagValuesRequest valuesRequest, Tag tag, String labelText) {
        if (Constants.INDENTATION.equals(valuesRequest.getIndentation())) {
            labelText = addIndentation(labelText, tag.getLocalTagID());
        }
        return labelText;
    }

    private String createLabelText(TagValuesRequest valuesRequest, Locale locale, Tag tag, String namespaceLabel) {
        String labelText;
        if (Constants.PATHLABEL.equals(valuesRequest.getLabelType())) {
            labelText = tag.getTitlePath(locale);
            labelText = labelText.replaceAll(namespaceLabel + " : ", "");
        } else {
            labelText = tag.getTitle(locale);
        }
        return labelText;
    }

    private String addIndentation(String label, String value) {
        int indent = StringUtils.countMatches(value, "/");
        String pre = "";
        for (int i = 0; i < indent; i++) {
            pre += "-";
            if (i == indent - 1) {
                pre += " ";
            }
        }
        return pre + label;
    }
}


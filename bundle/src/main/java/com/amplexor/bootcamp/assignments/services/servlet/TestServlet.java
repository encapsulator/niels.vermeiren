package com.amplexor.bootcamp.assignments.services.servlet;


import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import java.io.IOException;

@SlingServlet(paths = "/bin/bootcamp/test", methods = { "GET" })
public class TestServlet extends SlingAllMethodsServlet {

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.getWriter().println("{Status: WORKING}");
    }
}
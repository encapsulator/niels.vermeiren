package com.amplexor.bootcamp.assignments.services.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.function.BinaryOperator;

public class RequestUtil {

    public static String getOptionalParameter(SlingHttpServletRequest request, String name) throws ServletException, UnsupportedEncodingException {
        return getRequestParam(name,request);
    }

    public static String getRequiredParameter(SlingHttpServletRequest request, String name) throws ServletException, UnsupportedEncodingException {
        String value = getRequestParam(name, request);
        if (StringUtils.isBlank(value)) {
            throw new ServletException("Expected and required parameter " + name + " is missing or is blank");
        }
        return value;
    }

    private static String getRequestParam(String param, SlingHttpServletRequest request) throws UnsupportedEncodingException {
        String result = request.getParameter(param);
        if(StringUtils.isNotEmpty(result)) {
            //old code - perhaps still required - but currently removed because - was encoded in the product finder
            //http://daikinpublish01.dev.amplexor.com:4503/content/portal/en_US/home/applications/product-finder0.html##q=EHBC-B is directly changed to http://daikinpublish01.dev.amplexor.com:4503/content/portal/en_US/home/applications/product-finder0.html##q=EHBC%3B which results in the search box only fetching EHBC and dropping the rest
            //return new String(result.getBytes("iso-8859-1"), "UTF-8");
            return result;
        }
        return null;
    }
    
    public static String getRequestSuffix(SlingHttpServletRequest request) {
    	final RequestPathInfo requestPathInfo = request.getRequestPathInfo();
		String requestSuffix = StringUtils.EMPTY;
		
		if(requestPathInfo != null) {
			requestSuffix = getCleanRequestSuffix(requestPathInfo.getSuffix());
		}
		return requestSuffix;
    }
    
    private static String getCleanRequestSuffix(String requestSuffix) {
    	String cleanRequestSuffix = StringUtils.EMPTY;
		if(StringUtils.isNotBlank(requestSuffix)) {
			if(requestSuffix.startsWith("/")) {
				cleanRequestSuffix = requestSuffix.substring(1, requestSuffix.length());
			}
			cleanRequestSuffix = FilenameUtils.removeExtension(cleanRequestSuffix);
		}
		return cleanRequestSuffix;
    }

    public static <T> T getObjectFromRequest(SlingHttpServletRequest request, Class<T> classOfT) throws IOException {
        String body = request.getReader().lines().reduce("", new BinaryOperator<String>() {
            @Override
            public String apply(String current, String next) {
                return current + next;
            }
        });
        return JsonUtil.getObjectFromJson(body, classOfT);
    }

    public static void writeObjectToOutputWithoutAnnotation(SlingHttpServletResponse response, Object object) throws IOException {
        response.setContentType("application/json");
        response.getWriter().append(JsonUtil.getJsonFromObjectWithoutAnnotation(object));
        response.getWriter().flush();
    }
    
    public static <T> T getObjectFromRequestWithoutAnnotation(SlingHttpServletRequest request, Class<T> classOfT) throws IOException {
        String body = request.getReader().lines().reduce("", new BinaryOperator<String>() {
            @Override
            public String apply(String current, String next) {
                return current + next;
            }
        });
        return JsonUtil.getObjectFromJsonWithoutAnnotation(body, classOfT);
    }

    public static void writeObjectToOutput(SlingHttpServletResponse response, Object object) throws IOException {
        response.setContentType("application/json");
        response.getWriter().append(JsonUtil.getJsonFromObject(object));
        response.getWriter().flush();
    }
    
    public static void writeStringToOutput(SlingHttpServletResponse response, String json) throws IOException {
        response.setContentType("application/json");
        response.getWriter().append(json);
        response.getWriter().flush();
    }
}

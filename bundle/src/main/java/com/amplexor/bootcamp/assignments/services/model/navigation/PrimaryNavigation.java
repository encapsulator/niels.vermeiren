package com.amplexor.bootcamp.assignments.services.model.navigation;

import com.cognifide.slice.cq.qualifier.RequestedPage;
import com.cognifide.slice.mapper.annotation.SliceResource;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jeroen Pelt on 2-5-2016.
 * © AMPLEXOR International S.A. 2016 All rights reserved.
 */
@SliceResource
public class PrimaryNavigation {

    private static final int LANGUAGE_ROOT_LEVEL = 2;

    private Page currentPage;

    @Inject
    public PrimaryNavigation(@RequestedPage Page currentPage, PageManager pageManager) {
        this.currentPage = currentPage;
    }

    public Page getLanguageRootPage() {
        return currentPage.getAbsoluteParent(LANGUAGE_ROOT_LEVEL);
    }

    public List<Page> getFirstLevelChildren(){
        List<Page> children = new ArrayList<>();
        Page languageRootPage = getLanguageRootPage();
        if(languageRootPage != null) {
            Iterator<Page> iteratorChildern = languageRootPage.listChildren();
            while(iteratorChildern.hasNext()) {
                Page child = iteratorChildern.next();
                if(child.isHideInNav() != true) {
                    children.add(child);
                }
            }
        }
        return children;
    }

    private boolean isActive(Page navigationPage)  {
        //check on language level.
        if(getLanguageRootPage() != null) {
            String languageRootPath = getLanguageRootPage().getPath();
            if (navigationPage.getPath().contentEquals(languageRootPath) && currentPage.getPath().contentEquals(languageRootPath)) {
                return true;
            }

            //checks if navigationPage path matches currentPage path.
            if(currentPage.getPath().contains(navigationPage.getPath()) && !navigationPage.getPath().contentEquals(languageRootPath) ) {
                return true;
            }
        }
        return false;
    }

    public List<NavigationItem> getNavigation() {
        List<NavigationItem> navigationItems = new ArrayList<>();

        if(getLanguageRootPage() != null) {
            NavigationItem languageRootPage = new NavigationItem(getLanguageRootPage());
            languageRootPage.setNavigationActive(isActive(getLanguageRootPage()));
            navigationItems.add(languageRootPage);

            for (Page child : getFirstLevelChildren()) {
                NavigationItem childNavigationItem = new NavigationItem(child);
                childNavigationItem.setNavigationActive(isActive(child));
                navigationItems.add(childNavigationItem);
            }
        }

        return navigationItems;
    }


}

package com.amplexor.bootcamp.assignments.services.osgi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.cognifide.slice.api.injector.InjectorRunner;
import com.cognifide.slice.commons.SliceModulesFactory;
import com.cognifide.slice.cq.module.CQModulesFactory;

import com.google.inject.Module;

public class Activator implements BundleActivator {

    public static final String INJECTOR_NAME = "bootcamp-assignments-services";

    private static final String BUNDLE_NAME_FILTER = "com\\.amplexor\\.bootcamp-assignments-bundle";

    private static final String BASE_PACKAGE = "com.amplexor.bootcamp.assignments.services";

    @Override
    public void start(final BundleContext bundleContext) {

        final InjectorRunner injectorRunner = new InjectorRunner(bundleContext, INJECTOR_NAME,
                BUNDLE_NAME_FILTER, BASE_PACKAGE);

        final List<Module> sliceModules = SliceModulesFactory.createModules(bundleContext);
        // CQModulesFactory is a class coming from Slice Addons https://cognifide.atlassian.net/wiki/display/SLICE/Slice+CQ+Addons+-+4.1
        final List<Module> cqModules = CQModulesFactory.createModules();
        final List<Module> customModules = createCustomModules();

        injectorRunner.installModules(sliceModules);
        injectorRunner.installModules(cqModules);
        injectorRunner.installModules(customModules);

        try {
            injectorRunner.start();
        } catch (Exception exc) {
            Calendar now = new GregorianCalendar();
            System.out.println(now.getTime().toString() + " " + exc.getMessage());
        }
    }
    private List<Module> createCustomModules() {
        List<Module> applicationModules = new ArrayList<Module>();
        //populate the list with your modules
        return applicationModules;
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Calendar now = new GregorianCalendar();
        System.out.println(now.getTime().toString() + " " + "OSGi activator "+this.getClass().toString()+" has been stopped." );
    }
}
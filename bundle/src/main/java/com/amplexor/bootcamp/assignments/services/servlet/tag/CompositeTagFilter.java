package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by vincent.londersele on 08/09/14.
 */
final class CompositeTagFilter implements Filter<Tag> {

    private Set<Filter<Tag>> tagFilters = new HashSet<Filter<Tag>>();

    public CompositeTagFilter(Set<Filter<Tag>> tagFilters) {
        if (tagFilters != null) {
            this.tagFilters = tagFilters;
        }
    }

    @Override
    public boolean includes(Tag tag) {
        for (Filter<Tag> tagFilter : tagFilters) {
            if (!tagFilter.includes(tag)) {
                return false;
            }
        }
        return true;
    }

}

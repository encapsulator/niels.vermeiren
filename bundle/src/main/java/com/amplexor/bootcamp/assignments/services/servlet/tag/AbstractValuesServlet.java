package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.amplexor.bootcamp.assignments.services.util.Constants;
import com.amplexor.bootcamp.assignments.services.util.JsonUtil;
import com.amplexor.bootcamp.assignments.services.util.StringMapComparator;
import com.google.gson.stream.JsonWriter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class AbstractValuesServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 2543542623624158886L;

    private final static Logger logger = LoggerFactory.getLogger(AbstractValuesServlet.class);


    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        JsonUtil jsonUtils = new JsonUtil();
        getJson(request, response, jsonUtils);
    }

    protected abstract ValuesRequest createValuesRequest(SlingHttpServletRequest request);

    private void getJson(SlingHttpServletRequest request, SlingHttpServletResponse response, JsonUtil jsonUtils) throws IOException {
        ValuesRequest valuesRequest = createValuesRequest(request);
        if (valuesRequest == null) {
            jsonUtils.writeEmptyResponse(response.getWriter());
            jsonUtils.closeWriters(response.getWriter(), null);
            return;
        }
        getJson(response, valuesRequest, jsonUtils);
    }

    private void getJson(SlingHttpServletResponse response, ValuesRequest valuesRequest, JsonUtil jsonUtils) throws IOException {
        response.setCharacterEncoding(Constants.UTF_8);
		response.setContentType(Constants.APPLICATION_JSON);
        PrintWriter writer = response.getWriter();
        JsonWriter jsonWriter = null;
        try {
            List<Map<String, String>> results = getValues(valuesRequest);
            if (results == null || results.size() == 0) {
                jsonUtils.writeEmptyResponse(writer);
            } else {
                Collections.sort(results, new StringMapComparator(valuesRequest));
                jsonWriter = jsonUtils.writeJsonResponse(response, writer, results);
            }
        } catch (Exception e) {
            logger.warn(Constants.PROBLEM_WRITING_JSON, e);
            jsonUtils.writeEmptyResponse(writer);
        } finally {
            jsonUtils.closeWriters(writer, jsonWriter);
        }
    }

    protected abstract List<Map<String, String>> getValues(ValuesRequest vr);
}

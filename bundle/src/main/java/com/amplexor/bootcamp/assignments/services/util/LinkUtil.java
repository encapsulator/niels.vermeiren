package com.amplexor.bootcamp.assignments.services.util;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

public class LinkUtil {
    private static final String DOT_HTML = ".html";
    private static final String DOT_JSON = ".json";
    
    
    public static String externalUriWithHTMLExtension(Externalizer externalizer, ResourceResolver resourceResolver, String path) {
    	return externalUri(externalizer, resourceResolver, path, DOT_HTML);
    }
    
    private static String externalUri(Externalizer externalizer, ResourceResolver resourceResolver, String path, String extension) {
    	return externalizer.publishLink(resourceResolver, path) + extension;
    }

    public static String getLink(String location) {
        // Check if location is not blank
        // When the location starts with a '/' but not with '//' like for example '//www.google.be' (Protocol relative URI's)
        // We assume that the location is internal.
        if (StringUtils.isNotBlank(location) && (location.startsWith("/") && !location.startsWith("//"))) {
            location = location.concat(DOT_HTML);
        }
        return location;
    }

    public static boolean isAccessiblePagePath(final String pagePath, final ResourceResolver resourceResolver) {
        if (StringUtils.isNotEmpty(pagePath) && resourceResolver != null) {
            final Resource resource = resourceResolver.getResource(pagePath);
            if (resource != null) {
                final Page page = resource.adaptTo(Page.class);
                return (page != null && page.hasContent());
            }
        }
        return false;
    }

    public static String constructValidUrlFromPagePathWithPriorityToRedirectTarget(final String pagePath, final String unverifiedRedirectTarget, final ResourceResolver resourceResolver) {
        final String validatedRedirectTargetUrl = getValidatedRedirectTargetUrl(unverifiedRedirectTarget, resourceResolver);
        final String validatedPagePathUrl = constructValidUrlFromPagePath(pagePath, resourceResolver);
        return StringUtils.defaultIfEmpty(validatedRedirectTargetUrl, validatedPagePathUrl);
    }

    public static String constructValidUrlFromPagePath(final String pagePath, final ResourceResolver resourceResolver) {
        if (isAccessiblePagePath(pagePath, resourceResolver)) {
            return pagePath + DOT_HTML;
        }
        return StringUtils.EMPTY;
    }

    public static String getValidatedRedirectTargetUrl(final String givenRedirectPath, final ResourceResolver resourceResolver) {
        String redirect = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(givenRedirectPath)) {
            if (validURI(givenRedirectPath)) {
                redirect = givenRedirectPath;
            } else if (validResource(givenRedirectPath, resourceResolver)) {
                redirect = givenRedirectPath + DOT_HTML;
            }
        }
        return redirect;
    }

    private static boolean validResource(final String redirectTarget, final ResourceResolver resourceResolver) {
        return (resourceResolver != null && resourceResolver.isLive()) && (resourceResolver.resolve(redirectTarget) != null);
    }

    private static boolean validURI(String uri) {
        return uri.startsWith("http");
    }

}

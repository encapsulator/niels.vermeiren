package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.amplexor.bootcamp.assignments.services.util.Constants;
import org.apache.sling.api.SlingHttpServletRequest;

import java.util.Locale;
import java.util.ResourceBundle;

public abstract class ValuesRequest {

    private final SlingHttpServletRequest request;
    private final String[] selectors;
    private String language;
    private String order;
    private String labelId;
    private String labelText;

    public ValuesRequest(SlingHttpServletRequest request) {
        this.request = request;
        this.selectors = request.getRequestPathInfo().getSelectors();
        // The first selector is optional and contains the language. By default "en" is used.
        parseLanguageSelector();
        // The second selector is optional and declares the order (still needs to be implemented).
        parseOrderSelector();
        // The third selector is optional and declares the label for the id (in the JSON response). By default "value" is used.
        parseLabelIdSelector();
        // The fourth selector is optional and declares the label for the label (in the JSON response). By default "text" is used.
        parseLabelTextSelector();
    }


    public abstract ValuesRequestType getValuesRequestType();

    protected abstract void parseLabelTextSelector();

    protected abstract void parseLabelIdSelector();

    protected abstract void parseOrderSelector();

    protected abstract void parseLanguageSelector();

    //3
    protected void parseLabelTextSelector(final int labelTextSelectorId) {
        setLabelText(parseSelector(getSelectors(), labelTextSelectorId, Constants.DEFAULT_LABEL_TEXT));
    }

    //2
    protected void parseLabelIdSelector(final int labelIdSelectorId) {
        setLabelId(parseSelector(getSelectors(), labelIdSelectorId, Constants.DEFAULT_LABEL_ID));
    }

    //1
    protected void parseOrderSelector(final int orderSelectorId) {
        setOrder(parseSelector(getSelectors(), orderSelectorId, getValuesRequestType().equals(ValuesRequestType.COUNTRY) ? Constants.ASC_ORDER : Constants.DEFAULT));
    }

    //0
    protected void parseLanguageSelector(final int languageSelectorId) {
        setLanguage(parseSelector(getSelectors(), languageSelectorId, getValuesRequestType().equals(ValuesRequestType.COUNTRY) ? Constants.EN_LANGUAGE : Constants.DEFAULT));
    }

    protected String parseSelector(String[] selectors, int selectorId, String defaultSelector) {
        if (selectors.length > selectorId) {
            return selectors[selectorId];
        } else {
            return defaultSelector;
        }
    }

    public SlingHttpServletRequest getRequest() {
        return request;
    }

    public String getLanguage() {
        return language;
    }

    public String getOrder() {
        return order;
    }

    public String getLabelId() {
        return labelId;
    }

    public String getLabelText() {
        return labelText;
    }

    public Locale getLocale() {
        return new Locale(getLanguage());
    }

    public ResourceBundle getResourceBundle() {
        return request.getResourceBundle(getLocale());
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    protected final String[] getSelectors() {
        return selectors;
    }
}

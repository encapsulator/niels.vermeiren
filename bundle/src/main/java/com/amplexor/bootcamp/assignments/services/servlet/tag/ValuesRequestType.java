package com.amplexor.bootcamp.assignments.services.servlet.tag;

public enum ValuesRequestType {
    COUNTRY, TAG
}

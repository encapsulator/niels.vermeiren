package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.amplexor.bootcamp.assignments.services.util.Constants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Servlet params / URL selectors
 * 
 * 0: root tag
 * 1: language
 * 2: pathlabel or leaflabel
 * 3: depth (levels of tags hierarchy retrieved)
 * 4: sort order
 * 5: indentation
 * 6: label id
 * 7: label text
 * 8: namespace or title
 * 
 * Sample Url = http://localhost:4502/bin/corporate/tagvalues.corporate:country.en.pathlabel.1.text.json
 */

public class TagValuesRequest extends ValuesRequest {

    private final static Logger logger = LoggerFactory.getLogger(TagValuesRequest.class);

    public static TagValuesRequest create(SlingHttpServletRequest request) {
        String[] selectors = request.getRequestPathInfo().getSelectors();
        if (selectors == null || selectors.length == 0) {
            logger.warn("When requesting tag values there should be at least a single selector containing the namespace.");
            return null;
        }
        return new TagValuesRequest(request);
    }

    private String rootTag;
    private String labelType;
    private String suffix;
    private String indentation;
    private String valueFormaType;

    private int depth;

    public TagValuesRequest(SlingHttpServletRequest request) {
        super(request);
        // The first selector is mandatory and contains the requested rootTag.
        parseRootTagSelector();
        // The third selector is optional and declaress the type of label that is requested.
        // By default the complete path will be returned.
        parseLabelTypeSelector();
        // The fourth selector is optional and contains the requested depth.
        parseDepthSelector();

        parseValueFormatType();

        // The fifth selector is optional and declares the indentation.
        parseIndentationSelector();

        parseSuffix(request);
    }

    @Override
    public ValuesRequestType getValuesRequestType() {
        return ValuesRequestType.TAG;
    }


    private void parseValueFormatType() {
        if (getSelectors().length > 8 && Constants.NAMESPACE.equals(getSelectors()[8])) {
            this.valueFormaType = Constants.NAMESPACE;
        }
        if (getSelectors().length > 8 && Constants.TITLE.equals(getSelectors()[8])) {
            this.valueFormaType = Constants.TITLE;
        }
    }

    private void parseSuffix(SlingHttpServletRequest request) {
        this.suffix = request.getRequestPathInfo().getSuffix();
    }

    private void parseIndentationSelector() {
        this.indentation = parseSelector(getSelectors(), 5, Constants.DEFAULT);
    }

    private void parseDepthSelector() {
        this.depth = 1;
        if (getSelectors().length > 3) {
            if (Constants.INFINITY.equals(getSelectors()[3])) {
                this.depth = Integer.MAX_VALUE;
            } else {
                try {
                    this.depth = Integer.parseInt(getSelectors()[3]);
                } catch (NumberFormatException e) {
                    logger.warn("When requesting tag values up to a certain depth, this value should be an positive integer (defaulting to 1): {0}",
                            new Object[]{getSelectors()[3]});
                }
            }
        }
    }

    private void parseLabelTypeSelector() {
        if (getSelectors().length > 2 && Constants.LEAFLABEL.equals(getSelectors()[2])) {
            this.labelType = Constants.LEAFLABEL;
        } else {
            this.labelType = Constants.PATHLABEL;
        }
    }

    private void parseRootTagSelector() {
        this.rootTag = getSelectors()[0].replaceAll(":", "/");
        if (this.rootTag.indexOf("/") == -1) {
            // The given tag should represent a namespace.
            this.rootTag = this.rootTag + ":";
        } else {
            // The id of a tag should start with a namespace followed by colon.
            // Even if only the namespace is given, the colon needs to be set.
            this.rootTag = this.rootTag.replaceFirst("/", ":");
        }
    }

    @Override
    protected void parseLabelTextSelector() {
        parseLabelTextSelector(7);
    }

    @Override
    protected void parseLabelIdSelector() {
        parseLabelIdSelector(6);
    }

    @Override
    protected void parseOrderSelector() {
        parseOrderSelector(4);
    }

    @Override
    protected void parseLanguageSelector() {
        parseLanguageSelector(1);
    }

    public String getRootTag() {
        return rootTag;
    }

    public String getLabelType() {
        return labelType;
    }

    public int  getDepth() {
        return depth;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getIndentation() {
        return indentation;
    }

    public String getValueFormaType() {
        return valueFormaType;
    }
}

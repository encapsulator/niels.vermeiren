package com.amplexor.bootcamp.assignments.services.model.navigation;

import com.day.cq.wcm.api.Page;

/**
 * Created by Jeroen Pelt on 2-5-2016.
 * © AMPLEXOR International S.A. 2016 All rights reserved.
 */
public class NavigationItem {
    private boolean navigationActive = false;
    private Page navigationItem;

    public NavigationItem(Page navigationPage) {
        this.navigationItem = navigationPage;
    }

    public void setNavigationActive(boolean active) {
        this.navigationActive = active;
    }

    public boolean getNavigationActive() {
        return this.navigationActive;
    }

    public String getTitle() {
        return this.navigationItem.getTitle();
    }

    public String getPath() {
        return this.navigationItem.getPath();
    }
}

package com.amplexor.bootcamp.assignments.services.util;

import com.amplexor.bootcamp.assignments.services.servlet.tag.ValuesRequestType;
import com.amplexor.bootcamp.assignments.services.servlet.tag.ValuesRequest;


import java.util.Comparator;
import java.util.Map;

public class StringMapComparator implements Comparator<Map<String, String>> {

    private final ValuesRequest request;
    private final ValuesRequestType type;

    public StringMapComparator(ValuesRequest request) {
        this.request = request;
        this.type = request.getValuesRequestType();
    }

    @Override
    public int compare(Map<String, String> o1, Map<String, String> o2) {
        if (type.equals(ValuesRequestType.COUNTRY)) {
            if (isOrderDescending()) {
                return compareLabelText(o1, o2);
            } else {
                return compareLabelText(o2, o1);
            }
        } else if (type.equals(ValuesRequestType.TAG)) {
            // TODO Currently the "order" selector is not used. But this would be the place to do it.
            if (isOrderDefault()) {
                return compareLabelText(o1, o2);
            } else {
                return compareOrder(o1, o2);
            }
        }
        return -1;
    }

    private int compareLabelText(Map<String, String> stringMap, Map<String, String> anotherStringMap) {
        return anotherStringMap.get(request.getLabelText()).compareTo(stringMap.get(request.getLabelText()));
    }

    private boolean isOrderDescending() {
        return Constants.DESC_ORDER.equalsIgnoreCase(request.getOrder());
    }

    private boolean isOrderDefault() {
        return Constants.DEFAULT.equalsIgnoreCase(request.getOrder());
    }

    private int compareOrder(Map<String, String> stringMap, Map<String, String> anotherStringMap) {
        return stringMap.get(request.getOrder()).compareTo(anotherStringMap.get(request.getOrder()));
    }

}

package com.amplexor.bootcamp.assignments.services.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Utilities for json requests and responses
 */
public class JsonUtil {

    public static String getJsonStatusOkMessage() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "ok");
        return jsonObject.toString();
    }

    public static String getJsonFromObject(Object results) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(results);
    }

    public static <T> T getObjectFromJson(String json, Class<T> classOfT) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(json, classOfT);
    }
    
    public static String getJsonFromObjectWithoutAnnotation(Object results) {
    	Gson gson = new Gson();
        return gson.toJson(results);
    }

    public static <T> T getObjectFromJsonWithoutAnnotation(String json, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }
    
    public static Map<String, String> getMappingFromJsonConfig(String jsonString) {
    	Gson gson = new Gson();
    	Type stringStringMap = new TypeToken<Map<String, String>>(){}.getType();
    	return gson.fromJson(jsonString, stringStringMap);
    }
    
    public void writeEmptyResponse(PrintWriter writer) throws IOException {
        writer.println(Constants.EMPTY_JSON_ARRAY);
        writer.flush();
    }
    
    public JsonWriter writeJsonResponse(SlingHttpServletResponse response, PrintWriter writer, List<Map<String, String>> results) throws JSONException {
        setJsonResponseType(response);
        JsonWriter jsonWriter = new JsonWriter(writer);
        writeJsonValues(results, jsonWriter);
        return jsonWriter;
    }
    
    public void closeWriters(PrintWriter writer, JsonWriter jsonWriter) throws IOException {
        if (jsonWriter != null)
            jsonWriter.close();
        else
            writer.close();
    }
    
    public void setJsonResponseType(SlingHttpServletResponse response) {
        response.setCharacterEncoding(Constants.UTF_8);
        response.setContentType(Constants.APPLICATION_JSON);
    }    
    
    public void writeJsonValues(List<Map<String, String>> results, JsonWriter jsonWriter) throws JSONException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Type jsonType = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        gson.toJson(results, jsonType, jsonWriter);
    }    
}

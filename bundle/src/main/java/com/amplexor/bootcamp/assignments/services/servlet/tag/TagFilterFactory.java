package com.amplexor.bootcamp.assignments.services.servlet.tag;

import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;
import org.apache.sling.api.resource.ResourceResolver;

public interface TagFilterFactory {

    Filter<Tag> create(String suffix, ResourceResolver rr);

}

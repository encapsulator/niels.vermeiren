package com.amplexor.bootcamp.assignments.services.util;

public final class Constants {
	//FILE EXTENIONS
    public static final String DOT_IMG_PNG = ".img.png";
    public static final String DOT_IMG_JPG = ".img.jpg";
    public static final String DOT_HTML = ".html";
    
    
    public static final String BGIMAGE_NAME = "bgimage";
    public static final String IMAGE_NAME = "image";
    public static final String IMAGE_FILE_NAME = "file";
    public static final String EN_LANGUAGE 	= "en";
    public static final String PATHLABEL 	= "pathlabel";
    public static final String TITLE = "title";
    public static final String NAMESPACE = "namespace";
    public static final String INDENTATION = "indentation";
    public static final String DESC_ORDER = "desc";
    public static final String ASC_ORDER = "asc";    
    public static final String DEFAULT = "default";
    public static final String EMPTY_LINK = "#";
    public static final String PROBLEM_WRITING_JSON = "problem writing json";
    public static final String DEFAULT_LABEL_ID = "value";    
    public static final String DEFAULT_LABEL_TEXT = "text";
    public static final String INFINITY = "infinity";
    public static final String LEAFLABEL = "leaflabel";
    public static final String EMPTY_JSON_ARRAY = "[{}]";
    public static final String DEFAULT_SOLUTION_CLASSNAME = "gc";
    
    //ENCODING
    public static final String UTF_8 = "UTF-8";
    
    //JCR AND CQ
    public static final String JCR_CONTENT = "jcr:content";
    public static final String JCR_MIMETYPE = "jcr:mimeType";  
    public static final String JCR_TITLE = "jcr:title";  
    public static final String JCR_CREATED = "jcr:created";
    public static final String CQ_LASTMODIFIED = "cq:lastModified";
    public static final String CQ_PAGE = "cq:Page";
    
    //MIMETYPES
    public static final String MIME_JPEG = "image/jpeg";   
    public static final String MIME_PNG = "image/png";     
    public static final String APPLICATION_JSON = "application/json";
	
	//PATHS
    public static final String DEFAULT_LOGOPATH = "/etc/designs/bootcamp-assignments/clientlib/images/Amplexor-logo.png";
	public static final String DEFAULT_IMAGE_EXTENSION = ".img.png";
	public static final String DEFAULT_PAGE_IMAGE_PATH = "/image/file" + DEFAULT_IMAGE_EXTENSION;
	public static final String IMAGE_PLACEHOLDER_PATH = "/etc/designs/bootcamp-assignments/clientlib/images/placeholder.png";
	
	//TOPICS
	public static final String MYINSIGHT_LICSENSE_TOPIC = "com/amplexor/myinsight/license/request";
}

package com.amplexor.bootcamp.assignments.services.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateLocaleFormatUtil {
	private final static Logger LOGGER = LoggerFactory.getLogger(DateLocaleFormatUtil.class);
	private static String DDMMMMYYYY= "dd MMMM yyyy";
	private static String MMMMDDYYYY= "MMMM dd, yyyy";
	private static String YYYYMMMMDD= "yyyy MMMM dd";
	
	/**
	 * Returning a {@link String} Date Format based on the {@link Locale}.
	 * @param locale
	 * @return String Date Format
	 */
	public static String resolveDateFormat(Locale locale) {
		switch (locale.toLanguageTag()) {

		case "en-US":
			return MMMMDDYYYY;

		case "zh":
		case "zh-HK":
		case "zh-TW":
		case "zh-SG":
		case "zh-CN":
		case "hu-HU":
		case "ja":
		case "ja-JP-u-ca-japanese-x-lvariant-JP":
		case "ja-JP":
		case "ko":
		case "ko-KR":
		case "sv-SE":
			return YYYYMMMMDD;

		default:
			return DDMMMMYYYY;
		}
	}

	/**
	 * Return a localized {@link String}. created from a {@link Calendar} or {@link Date} Object.
	 * @param start - a {@link Calendar} or {@link Date} Object.
	 * @param end - a {@link Calendar} or {@link Date} Object.
	 * @param locale - {@link Locale} that must be used
	 * @return localized {@link String} with the start and end date.
	 */
	public static String formatEventDateFromObjects(Object start, Object end, Locale locale) {
		String formattedDate = "";
		Calendar calStart = null;
		Calendar calEnd = null;
		
		if(start != null) { 
			if(start instanceof Calendar) {
				calStart = (Calendar) start;
			}
			if(start instanceof Date) {
				calStart = (Calendar) Calendar.getInstance(locale);
				calStart.setTime((Date)start);
			}
		}
		
		if(end != null) {
			if(end instanceof Calendar) {
				calEnd = (Calendar) end;
			}
			if(end instanceof Date) {
				calEnd = (Calendar) Calendar.getInstance(locale);
				calEnd.setTime((Date)end);
			}
		} else if (start != null && calEnd == null ) {
			// No end date set, this will be a one day event!
			calEnd = (Calendar) Calendar.getInstance(locale);
			calEnd.setTime((Date)start);
		}
		
		
		if(calStart != null && calEnd != null) {
			if(calStart.after(calEnd)) {
				//misconfiguration
				LOGGER.info("Detected that calEnd is start date and calStart is end date." );
				if(!calEnd.getTimeZone().getID().contentEquals(calStart.getTimeZone().getID())) {
					LOGGER.error("Detected an TimeZonde difference." );
				}
				formattedDate = formatEventDateFromCalendar(calEnd, calStart, locale);
			}
			else {
				if(!calEnd.getTimeZone().getID().contentEquals(calStart.getTimeZone().getID())) {
					LOGGER.error("Detected an TimeZonde difference." );
				}
				formattedDate = formatEventDateFromCalendar(calStart, calEnd, locale);
			}
		}
		
		return formattedDate;
	}
	
	private static String formatEventDateFromCalendar(Calendar start, Calendar end, Locale locale) {
		boolean isSameDay = false;
		boolean isSameMonth = false;
		boolean isSameYear = false;

		SimpleDateFormat sdfDay = new SimpleDateFormat("dd", locale);
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM", locale);
		SimpleDateFormat sdfFullMonth = new SimpleDateFormat("MMMM", locale);
		SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy", locale);
		
		sdfDay.setTimeZone(start.getTimeZone());
		sdfMonth.setTimeZone(start.getTimeZone());
		sdfFullMonth.setTimeZone(start.getTimeZone());
		sdfYear.setTimeZone(start.getTimeZone());
		
		String startDayString = sdfDay.format(start.getTime());
		String startMonthString = sdfFullMonth.format(start.getTime());
		String startYearString = sdfYear.format(start.getTime());
		
		String endDayString = sdfDay.format(end.getTime());
		String endMonthString = sdfFullMonth.format(end.getTime());
		String endYearString = sdfYear.format(end.getTime());
		
		
		int startDay = Integer.parseInt(startDayString);
		int startMonth = Integer.parseInt(sdfMonth.format(start.getTime()));
		int startYear = Integer.parseInt(startYearString);
		
		int endDay = Integer.parseInt(endDayString);
		int endMonth = Integer.parseInt(sdfMonth.format(end.getTime()));
		int endYear = Integer.parseInt(endYearString);
		
		if(startYear == endYear) {
			isSameYear = true;
		}
		
		if(startMonth == endMonth) {
			isSameMonth = true;
		}
		
		if(startDay == endDay) {
			isSameDay = true;
		}
		
		String format = resolveDateFormat(locale);
		StringBuilder formatedString = new StringBuilder();
		
		if(format.contentEquals(YYYYMMMMDD)) {
			if(isSameYear) {
				if(isSameMonth) {
					if(isSameDay) {
						formatedString.append(startYearString);
						formatedString.append(" ");
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startDayString);
					}
					else {
						formatedString.append(startYearString);
						formatedString.append(" ");
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startDayString);
						formatedString.append(" - ");
						formatedString.append(endDayString);
					}
				}
				else {
					formatedString.append(startYearString);
					formatedString.append(" ");
					formatedString.append(startMonthString);
					formatedString.append(" ");
					formatedString.append(startDayString);
					formatedString.append(" - ");
					formatedString.append(endMonthString);
					formatedString.append(" ");
					formatedString.append(endDayString);
				}
			} else {
				formatedString.append(startYearString);
				formatedString.append(" ");
				formatedString.append(startMonthString);
				formatedString.append(" ");
				formatedString.append(startDayString);
				formatedString.append(" - ");
				formatedString.append(endYearString);
				formatedString.append(" ");
				formatedString.append(endMonthString);
				formatedString.append(" ");
				formatedString.append(endDayString);
			}
		}
		
		if(format.contentEquals(MMMMDDYYYY)) {
			if(isSameYear) {
				if(isSameMonth) {
					if(isSameDay) {
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startDayString);
						formatedString.append(", ");
						formatedString.append(startYearString);
					}
					else {
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startDayString);
						formatedString.append(" - ");
						formatedString.append(endDayString);
						formatedString.append(", ");
						formatedString.append(startYearString);
					}
				}
				else {
					formatedString.append(startMonthString);
					formatedString.append(" ");
					formatedString.append(startDayString);
					formatedString.append(" - ");
					formatedString.append(endMonthString);
					formatedString.append(" ");
					formatedString.append(endDayString);
					formatedString.append(", ");
					formatedString.append(startYearString);
				}
			} else {
				formatedString.append(startMonthString);
				formatedString.append(" ");
				formatedString.append(startDayString);
				formatedString.append(", ");
				formatedString.append(startYearString);
				formatedString.append(" - ");
				formatedString.append(endMonthString);
				formatedString.append(" ");
				formatedString.append(endDayString);
				formatedString.append(", ");
				formatedString.append(endYearString);
			}
		}
		
		if(format.contentEquals(DDMMMMYYYY)) {
			if(isSameYear) {
				if(isSameMonth) {
					if(isSameDay) {
						formatedString.append(startDayString);
						formatedString.append(" ");
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startYearString);
					}
					else {
						formatedString.append(startDayString);
						formatedString.append(" - ");
						formatedString.append(endDayString);
						formatedString.append(" ");
						formatedString.append(startMonthString);
						formatedString.append(" ");
						formatedString.append(startYearString);
					}
				}
				else {
					formatedString.append(startDayString);
					formatedString.append(" ");
					formatedString.append(startMonthString);
					formatedString.append(" - ");
					formatedString.append(endDayString);
					formatedString.append(" ");
					formatedString.append(endMonthString);
					formatedString.append(" ");
					formatedString.append(startYearString);
				}
			} else {
				formatedString.append(startDayString);
				formatedString.append(" ");
				formatedString.append(startMonthString);
				formatedString.append(" ");
				formatedString.append(startYearString);
				formatedString.append(" - ");
				formatedString.append(endDayString);
				formatedString.append(" ");
				formatedString.append(endMonthString);
				formatedString.append(" ");
				formatedString.append(endYearString);
			}
		}
		
		return formatedString.toString();
	}
	
}

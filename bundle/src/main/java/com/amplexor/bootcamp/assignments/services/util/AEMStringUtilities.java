package com.amplexor.bootcamp.assignments.services.util;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AEMStringUtilities {
	
    public static String getValidInternalUrl(String url, Page requestedPage, PageManager pageManager) {
        return getValidInternalUrl(url,  requestedPage,  pageManager, true);
    }
    
	public static String getValidInternalUrl(String url, Page requestedPage, PageManager pageManager, boolean appendSuffix) {
		List<String> validSuffixList   = Arrays.asList(".htm", ".html", ".jsp", ".doc", ".docx", ".pdf", ".pptx", ".ppt", ".ppsx", ".odp", ".xls", ".xlsx", ".csv", ".xml", ".txt", ".zip", ".rar");

		if (StringUtils.isBlank(url)) {
			return "";
		}

		if (url.endsWith("/jcr:content")) {
			url = url.replace("/jcr:content", "");
		}
		
        if(requestedPage != null) {
            url = updateIntUrlLocale(url, requestedPage, pageManager);
        }
        
		StringBuilder sb = new StringBuilder(url);
		
        
        if(appendSuffix) {
            // check if url ends with valid html suffix
            boolean hasValidSuffix = false;
            for (String validSuffix : validSuffixList) {
                if (url.toLowerCase().endsWith(validSuffix)) {
                    hasValidSuffix = true;
                    break;
                }
            }
            if (!hasValidSuffix) {

                sb.append(".html");
            }	
        }
		
		return sb.toString();
		
	}
    
    public static String getValidInternalUrl(String url) {
        return getValidInternalUrl(url, null, null);
    }
	
	public static String getValidExternalUrl(String url) {
		
		List<String> validProtocolList = Arrays.asList("http://", "https://", "file://", "ftp://");

		if(StringUtils.isBlank(url)) {
			return "";
		}

		StringBuilder sb = new StringBuilder(url);
		
		// check if url starts with valid protocol
		boolean hasValidProtocol = false;
		for (String validProtocol : validProtocolList) {
			if (url.toLowerCase().startsWith(validProtocol)) {
				hasValidProtocol = true;
				break;
			}
		}
		
		if (!hasValidProtocol) {
			if (url.startsWith("/")) {
				sb.insert(0, "http:/");
			} else {
				sb.insert(0, "http://");
			}
		}	
		
		return sb.toString();
	}	

    public static String updateIntUrlLocale(String intUrl, String locale, PageManager pageManager) {
        if (!intUrl.contains("/" + locale + "/") && !intUrl.endsWith("/" + locale)) {
            String[] parts = intUrl.split("/");
            String tempIntUrl = "";
            
            String firstPart = parts[1] + "/" + parts[2]  + "/";
            
            if (parts.length == 4) {
                tempIntUrl = intUrl.replaceFirst(firstPart + parts[3], firstPart + locale );
            }
            else if (parts.length > 4) {
                tempIntUrl = intUrl.replaceFirst(firstPart + parts[3] + "/", firstPart + locale + "/");
            }
             
            Page page = pageManager.getPage(tempIntUrl);
            if (page != null) {
                intUrl = tempIntUrl;
            }
            
        }
        return intUrl;
    }
    
    public static String updateIntUrlLocale(String intUrl, Page requestedPage, PageManager pageManager) {
        String locale = getLocaleFromPage(requestedPage);
        
        return updateIntUrlLocale(intUrl, locale, pageManager);
    }
    
    public static String getLocaleFromPage(Page requestedPage) {
        Locale localeObj = requestedPage.getLanguage(true);
    	return localeObj.getLanguage();
    }
    
		
}
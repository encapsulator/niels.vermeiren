package com.amplexor.bootcamp.assignments.services.model.title;

import com.amplexor.bootcamp.assignments.services.util.AEMStringUtilities;
import com.cognifide.slice.cq.qualifier.RequestedPage;
import com.cognifide.slice.mapper.annotation.JcrProperty;
import com.cognifide.slice.mapper.annotation.SliceResource;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

@SliceResource
public class Title {

    private static final String DEFAULT_LINK = "";
    private static final String DEFAULT_LABEL = "bootcamp-assignments.component.title.default";
    private static final String DEFAULT_SUBTITLE_SIZE = "h5";

    @JcrProperty
    private String titleText;

    @JcrProperty
    private String titleTextLabel;

    @JcrProperty
    private String subtitleText;

    @JcrProperty
    private String subtitleTextLabel;

    @JcrProperty
    private String titleSize;

    @JcrProperty
    private String titleStyle;

    @JcrProperty
    private String subtitleSize;

    @JcrProperty
    private String intUrl;

    @JcrProperty
    private String extUrl;

    @JcrProperty
    private String titleArces;

    @JcrProperty
    private String titleColor;

    @JcrProperty
    private boolean titleUppercase;

    private final String DEFAULT_TITLE_COLOR_CLASS = "primary-color";
    private final String DEFAULT_TITLE_UPPERCASE_CLASS = "uppercase";

    private PageManager pageManager;
    private Page currentPage;

    @Inject
    public Title(@RequestedPage Page currentPage, PageManager pageManager) {
        this.currentPage = currentPage;
        this.pageManager = pageManager;
    }

    public String getStyle() {
        StringBuilder style = new StringBuilder();

        if (!StringUtils.isEmpty(titleStyle)) {
            style.append(titleStyle);
            style.append(" ");
        }

        if (!StringUtils.isEmpty(titleArces)) {
            style.append(titleArces);
            style.append(" ");
        }

        if (!StringUtils.isEmpty(titleColor)) {
            style.append(titleColor);
            style.append(" ");
        } else {
            style.append(DEFAULT_TITLE_COLOR_CLASS);
            style.append(" ");
        }

        if (titleUppercase) {
            style.append(DEFAULT_TITLE_UPPERCASE_CLASS);
            style.append(" ");
        }

        return style.toString();
    }

    public String getSize() {
        return StringUtils.isEmpty(titleSize) ? "" : titleSize;
    }

    public String getSubtitleSize() {
        return StringUtils.isEmpty(subtitleSize) ? DEFAULT_SUBTITLE_SIZE : subtitleSize;
    }

    public String getTitleText() {
        return StringUtils.isEmpty(titleText) ? "" : titleText;
    }

    public String getI18nTitleLabel() {
        if (titleTextLabel != null) {
            //Removing /apps/bootcamp-assignments/i18n/en/ from /apps/bootcamp-assignments/i18n/en/bootcamp-assignments.cookie.banner.learnmore
            if (titleTextLabel.contains("/")) {
                String[] str_parts = titleTextLabel.split("/");
                return str_parts[str_parts.length - 1];
            }
            return titleTextLabel;
        }

        return DEFAULT_LABEL;
    }

    public String getSubtext() {
        return StringUtils.isEmpty(subtitleText) ? "" : subtitleText;
    }

    public String getI18nSubtitleLabel() {
        if (subtitleTextLabel != null) {
            //Removing /apps/bootcamp-assignments/i18n/en/ from /apps/bootcamp-assignments/i18n/en/bootcamp-assignments.cookie.banner.learnmore
            if (subtitleTextLabel.contains("/")) {
                String[] str_parts = subtitleTextLabel.split("/");
                return str_parts[str_parts.length - 1];
            }
            return subtitleTextLabel;
        }

        return "";
    }

    public String getHref() {
        if (intUrl != null) {
            return AEMStringUtilities.getValidInternalUrl(intUrl, currentPage, pageManager);
        }

        if (extUrl != null) {
            return AEMStringUtilities.getValidExternalUrl(extUrl);
        }

        return DEFAULT_LINK;
    }
}

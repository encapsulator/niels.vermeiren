# AEM Assignments Project
This project where the students create their assignments in.

## Startup / Dependencies
Run your AEM instance and finish the startup.

Please make sure that the following packages are installed on your AEM instance (via <http://localhost:4502/crx/packmgr> ):
* AEM-6.1-Service-Pack-1-6.1.SP1.zip
* cq-6.1.0-hotfix-9084-1.0.zip
* acs-aem-commons-content-2.4.2.zip (version may vary)
* com.adobe.acs.bundles.twitter4j-content-1.0.0.zip

## Build This Project
You can build this project with the following command:

~~~~
mvn clean install -PautoInstallPackage
~~~~

## Errors

### org.apache.sling.scripting.sightly.SightlyException: cannot be correctly instantiated by the Use API
If the bundle cannot be correctly instantiated by the Use API. 
Try to restart you AEM server and check on http://localhost:4502/system/console/bundles if the "peaberry - Dynamic services for Google-Guiceorg.ops4j.peaberry" is active.

## Contributors
This project has been created by:
* jeroen.pelt@amplexor.com
* jurgen.brouwer@amplexor.com

